CSS Only Bootstrap Tabs
=======================

Here's the thing: why would you use a script language and load huge libraries for it when there's no need at all? I
was looking for a CSS only implementation for tabs, that are compatible with Bootstrap's `nav-tabs`, but found none.
Even the ones I've found used anchors, which scrolled the page on every click, hiding the tab panel. That's awfully
bad and uncomfortable to use. So I've decided to create a proper solution, that does not use javascript yet looks and
works exactly as the original.

There's a downside though, you have to know the maximum number of tabs in advance. Not a big deal I think. In return
you'll get minimal CSS, clean HTML and smooth, intuitive user experience. I'd say that worths it!

HTML
----

Let's see the HTML for an example tab panel, which is almost the same as the original, javascript and jQuery bloated
Bootstrap version:

```html
<!-- required for CSS selectors -->
<input type="radio" name="tab" id="tab0" checked>
<input type="radio" name="tab" id="tab1">
<input type="radio" name="tab" id="tab2">
<input type="radio" name="tab" id="tab3">

<!-- the tab panel -->
<ul class="nav nav-tabs">
    <li class="nav-link" rel="tab0">
        <label for="tab0">Tab 1</label>
    </li>
    <li class="nav-link" rel="tab1">
        <label for="tab1">Tab 2</label>
    </li>
    <li class="nav-link" rel="tab2">
        <label for="tab2">Tab 3</label>
    </li>
    <li class="nav-link" rel="tab3">
        <label for="tab3">Tab 4</label>
    </li>
</ul>

<!-- contents for each tab -->
<div rel="tab0">
    tab content 0
</div>
<div rel="tab1">
    tab content 1
</div>
<div rel="tab2">
    tab content 2
</div>
<div rel="tab3">
    tab content 3
</div>
```

In the first block we define as many radio buttons as the number of tabs, which must not exceed the number of selectors
defined in CSS (see below). I'd like to have the boxes next to their label, but unfortunatelly CSS3 does not support
"parent" or "has a certain child" selectors. So input boxes must preceed the tab panel and the divs for the tabs.

The second block is a typical Bootstrap `nav-tabs` list, with minor modifications: instead of adding "active" to one
of the li's class, we tell each li element which tab they relate to. It's done by using the "rel" attribute and the
id of the corresponding input box. And as you can see, we can set the active tab by checking one of the input boxes.

Finally we have a div element for each tab with their contents, using the same "rel" attribute.

Nice and clean HTML, don't you think?

CSS
---

And here's the stylesheet that brings the tabs alive without a single line of javascript:

```css
.nav-tabs li {
    position: relative;
    bottom: -1px;
}
.nav-tabs li label {
    margin-bottom: 0px !important;
    cursor: inherit;
}
input[name="tab"], div[rel^="tab"] {
    display: none;
}
```

We need to fix some things in Bootstrap. The `nav-tabs` class sets a 1 pixel thin bottom border on the ul element, which
we want to hide for the active tab, so move all li elements down by one pixel. Next, we clear the bottom margin for the
label inside li elements, and we tell them to dynamically inherit the cursor of the parent. Then we have to hide the
input boxes (required only to describe selectors), and we also must hide all the tab contents by default. And finally
comes the selector magic:

```css
#tab0:checked ~ ul li[rel="tab0"],
#tab1:checked ~ ul li[rel="tab1"],
#tab2:checked ~ ul li[rel="tab2"],
#tab3:checked ~ ul li[rel="tab3"],
#tab4:checked ~ ul li[rel="tab4"],
#tab5:checked ~ ul li[rel="tab5"],
#tab6:checked ~ ul li[rel="tab6"],
#tab7:checked ~ ul li[rel="tab7"] {
    border-color: #dee2e6 #dee2e6 #fff;
    cursor: default;
}

#tab0:checked ~ div[rel="tab0"],
#tab1:checked ~ div[rel="tab1"],
#tab2:checked ~ div[rel="tab2"],
#tab3:checked ~ div[rel="tab3"],
#tab4:checked ~ div[rel="tab4"],
#tab5:checked ~ div[rel="tab5"],
#tab6:checked ~ div[rel="tab6"],
#tab7:checked ~ div[rel="tab7"] {
    display: block;
}
```

The first block draws a border around the active tab (same style as `nav-link active`), plus we turn off pointer
cursor for better user experience. The second box is similar, but that displays the contents of the active tab.

As I've mentioned before, you have to specify as many selectors as the biggests number of tabs you want to support
within a single tab panel on your site.

Hope this was useful, enjoy!
bzt
